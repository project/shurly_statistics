<?php
// $Id: shurly.views.inc,v 1.1 2010/08/17 15:57:52 jjeff Exp $

/**
 * @file Shurly Views data include file
 */

function shurly_statistics_views_data() {

  // Basic table information.

  $data['shurly_statistics']['table']['group']  = t('Shurly Statistics');
  
  $data['shurly_statistics']['table']['join'] = array(
    'shurly' => array(
      'left_field' => 'rid',
      'field' => 'rid',
    ),
  );

  $data['shurly_statistics']['id'] = array(
    'title' => t('Access ID Number'),
    'help' => t('References an spcific access number on a ShURLy link'),
    'field' => array(
       'handler' => 'views_handler_field_numeric',
       'click sortable' => TRUE,
      ),
    'filter' => array(
       'handler' => 'views_handler_filter_numeric',
      ),
    'sort' => array(
       'handler' => 'views_handler_sort',
      ),
    'argument' => array(
       'handler' => 'views_handler_argument_numeric',
    ),
  );

  $data['shurly_statistics']['rid'] = array(
    'title' => t('ShURLy reference ID'),
    'help' => t('References an spcific ShURLy link'),
    'field' => array(
       'handler' => 'views_handler_field_numeric',
       'click sortable' => TRUE,
      ),
    'filter' => array(
       'handler' => 'views_handler_filter_numeric',
      ),
    'sort' => array(
       'handler' => 'views_handler_sort',
      ),
    'argument' => array(
       'handler' => 'views_handler_argument_numeric',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'shurly',
      'base field' => 'rid',
      'label' => t('rid'),
    ),
  );

  $data['shurly_statistics']['remote_ip'] = array(
    'title' => t('IP address'),
    'help' => t('The Remote IP address that clicked the link.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['shurly_statistics']['location'] = array(
    'title' => t('location'),
    'help' => t('The geo location of remote IP.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['shurly_statistics']['referer'] = array(
    'title' => t('refering url'),
    'help' => t('The URL that refered to this link'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['shurly_statistics']['browser'] = array(
    'title' => t('Browser'),
    'help' => t('The browser name.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['shurly_statistics']['version'] = array(
    'title' => t('Version'),
    'help' => t('The browser version used.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['shurly_statistics']['platform'] = array(
    'title' => t('Platform'),
    'help' => t('The Platform (OS) used.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  // Link last used timestamp
  $data['shurly_statistics']['access_time'] = array(
    'title' => t('Access time'),
    'help' => t('The specific date/time the link was used.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );
  /*
  $data['shurly_statistics']['shurly_statistics_info'] = array(
    'field' => array(
      'title' => t('Statistics Link'),
      'help' => t('A link to display click statistics'),
      'handler' => 'shurly_statistics_handler_field_shurly_statistics_info',
      'parent' => 'views_handler_field',
    ),
  );
  */
  return $data;
}


/**
 * Implementation of hook_views_handlers().
 *
function shurly_statistics_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'shurly_statistics') . '/views',
    ),
    'handlers' => array(
      'shurly_statistics_handler_field_shurly_statistics_info' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
 */